/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./app/**/*.{js,ts,jsx,tsx,mdx}",
    "./pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./components/**/*.{js,ts,jsx,tsx,mdx}",
 
    // Or if using `src` directory:
    "./src/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      transform: ['hover', 'focus'],
      rotate: {
        '360': '360deg',
      },
      backgroundImage: {
        'bg-header': "url('/images/header/bg-header.png')",
      },
      fontSize: {
        'text-h1': ['22px'],
        'text-h2': ['24px'],
        'text-h4': ['18px'],
        'menu': ['18px']
      },
      colors: {
        primary: '#252525',
        secondary: '#f6931d',
        bgShadow: '#393e2f',
        bgMenu: '#222222',
        h1Color: '#996d29',
        h4Color: '#db7e26',
      }
    },
  },
  plugins: [],
}

