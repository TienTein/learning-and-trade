import React from "react";

const logoParner = [
    {
        src: '/images/logo/ancuong.svg'
    },
    {
        src: '/images/logo/malloca.svg'
    },
    {
        src: '/images/logo/aconcept.svg'
    },
    {
        src: '/images/logo/go.svg'
    },
    {
        src: '/images/logo/topen.svg'
    },
    {
        src: '/images/logo/ancuong.svg'
    },
    {
        src: '/images/logo/malloca.svg'
    },
    {
        src: '/images/logo/godere.svg'
    },
    {
        src: '/images/logo/bagac.svg'
    },
    {
        src: '/images/logo/godere.svg'
    },
    {
        src: '/images/logo/bagac.svg'
    },
    {
        src: '/images/logo/aconcept.svg'
    },
];

const Partner = () => {
    return (
        <>
            <div className="partner-area">
                <div className="partner-content">
                    <div className="container-fluid mb-5 mb-md-6">
                        <div className="row justify-content-md-center">
                            <div className="col-12 col-md-10 col-lg-8 col-xl-7 col-xxl-6 text-center">
                                <h2 className="">CÁC QUÝ ĐỐI TÁC CỦA CHÚNG TÔI</h2>
                            </div>
                        </div>
                    </div>
                    <div className="container overflow-hidden">
                        <div className="row">
                            {logoParner && logoParner.map((logo, i) => (
                                <div className="logo-item col-6 col-md-2 align-self-center text-center">
                                    <img src={logo.src} />
                                </div>
                            ))}
                        </div>
                    </div>
                </div>

                <div class='btn-come'>
                    <a class='flex items-center justify-center text-white text-center'>ĐẾN VỚI CHÚNG TÔI</a>
                </div>
            </div>
        </>
    );
};

export default Partner;