import React, { useState } from 'react'

const Dropdown = ({ options, onSelect }) => {
  const [selectedOption, setSelectedOption] = useState(null)

  const handleSelect = (option) => {
    setSelectedOption(option)
    onSelect(option)
  }

  return (
    <select
      value={selectedOption}
      onChange={(e) => handleSelect(e.target.value)}
      className="appearance-none  py-2 px-3 leading-tight text-white bg-transparent rounded-md focus:outline-none hover:underline" // Added border and border-black classes
    >
      {options.map((option) => (
        <option
          key={option}
          value={option}
          className="border-b border-black py-2 px-3 bg-gray-800 text-white"
        >
          {' '}
          {/* Added option styling */}
          {option}
        </option>
      ))}
    </select>
  )
}

export default Dropdown
