'use client'
import React from 'react'
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';

const ProductList = () => {
    return (
        <>
            <div className='h-screen pt-5 product-area'>
                <div className='container mx-auto mb-5 relative' style={{ zIndex: '1' }}>
                    <h1 className='pl-5'>SẢN PHẨM</h1>
                </div>
                
                <div className='relative product-items'>
                    <div className='items-bg'>
                        <img src='/images/product/bg-img-pro.png' />
                    </div>
                    <Swiper
                        slidesPerView={3}
                        loop={true}
                        spaceBetween={60}>
                        <div>
                            <SwiperSlide>
                                <div className='flex flex-col items-center pt-5'>
                                    <img className='w-full' src='/images/product/product1.png' />
                                    <h4 className='text-h4Color text-text-h4 font-medium uppercase'>Ấn phẩm</h4>
                                </div>
                            </SwiperSlide>
                            <SwiperSlide>
                                <div className='flex flex-col items-center'>
                                    <h4 className='text-h4Color text-text-h4 font-medium uppercase'>Bao bì - hộp giấy</h4>
                                    <img className='w-full' src='/images/product/product2.png' />
                                </div>
                            </SwiperSlide>
                            <SwiperSlide>
                                <div className='flex flex-col items-center pt-5'>
                                    <img className='w-full' src='/images/product/product3.png' />
                                    <h4 className='text-h4Color text-text-h4 font-medium uppercase'>Thiết bị quảng cáo</h4>
                                </div>
                            </SwiperSlide>
                            <SwiperSlide>
                                <div className='flex flex-col items-center'>
                                    <h4 className='text-h4Color text-text-h4 font-medium uppercase'>Bao bì - hộp giấy</h4>
                                    <img className='w-full' src='/images/product/product2.png' />
                                </div>
                            </SwiperSlide>
                        </div>
                    </Swiper>
                    
                    <div class='btn-see-more'>
                        <a class='flex items-center justify-center text-white text-center'>ĐẾN VỚI CHÚNG TÔI</a>
                    </div>
                </div>
            </div>
        </>
    )
}

export default ProductList